Usage
=====

.. _cmd:

struct of data
------------

.. _tab-super-result:

.. table:: Commands & Values.
+-------------------+--------------------+
| CMD               |  Value             |
+===================+====================+
| 'D': Device       | 'M': AC_Midea      | 
|                   |--------------------+
|                   | 'K': AC_Kelvinator |
+-------------------+--------------------+
| 'E': Command      | '0': Turn Off      | 
|                   |--------------------+
|                   | '1': Turn On       |
|                   |--------------------+
|                   | '2': Defaut Reset  |
+-------------------+--------------------+
| 'M': Mode         | '0': Automatic     | 
|                   |--------------------+
|                   | '1': Cool          |
|                   |--------------------+
|                   | '2': Heat          |
|                   |--------------------+
|                   | '3': Dry           |
|                   |--------------------+
|                   | '4': Fan           |
+-------------------+--------------------+
|'T':setTemperature |  16< value < 26    |
+-------------------+--------------------+
| 'E': Swing        | '0': Turn Off      | 
|                   |--------------------+
|                   | '1': Turn On       |
+-------------------+--------------------+
| 'F ':Velocity Fan | '0': Automatic     | 
|                   |--------------------+
|                   | '1': Min           |
|                   |--------------------+
|                   | '2': Med           |
|                   |--------------------+
|                   | '3': High          |
+-------------------+--------------------+
| 'L': Light        | '0': Turn Off      | 
|                   |--------------------+
|                   | '1': Turn On       |
+-------------------+--------------------+

+-------------------+--------------------+
|           Especial Commands:           |
+===================+====================+
| 'R': Reboot Dev   | '0': ESP Module    | 
|                   |--------------------+
|                   | '1': Temp Sensor   |
+-------------------+--------------------+
| 'G':GetTemp&Hum   | '0':resend request | 
+-------------------+--------------------+

Generating Msg
----------------
Ascii characters of 28 bytes.

+ Sample_1: **"D:K|E:1|M:1|T:22|F:0|S:0|L:1"**  -> this msg,  turn on AC Kelvinator, with mode 1, temperature 22°C, Fan mode automatic, Swing off, and Light on

+ Sample_2: **"D:M|E:0"**  -> this msg, turn off AC Midea

+ Sample_3: **"R:0"**  -> Reboot device ESP

+ Sample_4: **"G:0"**  -> Request temperature & humidity


Comunication
----------------

Timer on 3seg default, the device's sending the temperature & humidity each 3 seg. MSG ascii of 9 bytes: "T:XX|H:XX" 

Process to send Ir code to AC's:

* MAIN_CPU (send UDP command  "Sample_1") -> ESP_Module
* ESP_Module (Response UDP "ACK") -> MAIN_CPU


Process to Reqs sense Temp&Humd:

* MAIN_CPU (send UDP command  "Sample_4") -> ESP_Module
* ESP_Module (Response UDP **"T:XX|H:XX"**) -> MAIN_CPU
